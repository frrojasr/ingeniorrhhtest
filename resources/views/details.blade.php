<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Weather Cities</title>

        <!-- CSS de Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <h1>Weather Cities</h1>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <table id="table-ciudades" class="table table-hover table-striped table-bordered ">

                        <thead>
                            <tr class="active">
                                <th width="auto">Id</th>
                                <th width="auto">Ciudad</th>
                                <th width="auto">Temp.</th>
                                <th width="auto">Temp Max.</th>
                                <th width="auto">Temp.Min</th>
                                <th width="auto">Presión</th>
                                <th width="auto">Humedad</th>
                                <th width="auto">Ultima Actualización</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center; ">{{$city->id}}</td>
                                <td style="text-align: center; "><a href="/city/{{$city->name}}" target="_blank"> {{$city->name}}</a></td>
                                <td style="text-align: center; ">{{$city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first()->temp}}</td>
                                <td style="text-align: center; ">{{$city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first()->temp_max}}</td>
                                <td style="text-align: center; ">{{$city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first()->temp_min}}</td>
                                <td style="text-align: center; ">{{$city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first()->pressure}}</td>
                                <td style="text-align: center; ">{{$city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first()->humidity}}</td>  
                                <td style="text-align: center; ">{{$city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first()->date_fetch}}</td>
                            </tr>

                        </tbody>

                    </table>
                </div>

                <canvas id="canvas" height="280" width="600"></canvas>


            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
        
         <script>
//       
    //    var Horas = JSON.parse({{ $horas }});
//        
//        $.each(Horas, function (key, value) {
//                console.log(key, value)
//             
//            });
   
        var Labels =  [];
        var Temps =  [];
        
        $(document).ready(function(){
        
            var ctx = document.getElementById("canvas").getContext('2d');
                var myChart = new Chart(ctx, {
                  type: 'bar',
                  data: {
                      labels:[0,1,2,3,4],
                      datasets: [{
                          label: 'Temp Horas',
                          data: [1,2,3,4,5],
                          borderWidth: 1
                      }]
                  },
                  options: {
                      scales: {
                          yAxes: [{
                              ticks: {
                                  beginAtZero:true
                              }
                          }]
                      }
                  }
              });
          });
     
        </script>
        
        

    </body>
</html>