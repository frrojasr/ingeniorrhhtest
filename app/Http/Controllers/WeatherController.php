<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\WeatherMap\Entities\City;
use Illuminate\Support\Facades\DB;

class WeatherController extends Controller {

    //
    public function index() {

        $cities = City::All();


        return view('index', ['cities' => $cities]);
    }

    public function detail($id) {


        $city = City::Where('name', $id)->first();

        $horas = DB::select("select DATE_FORMAT(date_fetch, '%m/%d/%Y %H:%i') as fecha "
                        . "from citiesweathers "
                        . "where id_city = $city->id order by date_fetch asc");

        $arrHoras = [];
        foreach ($horas as $row) {
            $arrHoras[] = $row->fecha;
        }



        $temps = DB::select("select temp "
                        . "from citiesweathers "
                        . "where id_city = $city->id order by date_fetch asc");

        $arrTemp = [];
        foreach ($temps as $row) {
            $arrTemp[] = $row->temp;
        }

        $horas = json_encode($arrHoras);
        $temps = json_encode($arrTemp);      
        

        $cityHistory = $city->myWeatherRecords()->orderBy('date_fetch', 'desc')->get();

        return view('details', ['city' => $city, 'horas' => $horas, 'temps' => $temps]);
    }

}
