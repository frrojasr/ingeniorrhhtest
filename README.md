Esta app ha sido creada como prueba tecnica para el proceso de seleccion.

PHP VERSION 7.2
LARAVEL 6.18

INTRUCCIONES DE INSTALACION

1.- Clone el repositorio con el siguiente comando:
	
		$ git clone https://frrojasr@bitbucket.org/frrojasr/ingeniorrhhtest.git

2.- desde la consola del servidor web ejecute: 

			$ composer install
			$ cp .env.example .env
			$ php artisan key:generate
			
3.- Desde Mysql crear una nueva base de datos: weathermap


4.- Abrir archivo .env y configurar las credenciales de conexion a la base de datos

		DB_HOST=localhost
		DB_DATABASE=weathermap
		DB_USERNAME=root
		DB_PASSWORD=elpassword

5.- desde la consola del servidor web ejecute:
				
		$ php artisan migrate --seed
		
6.- Durante la etapa de testeo el servidor podra levantarse con el comando 
		
		$ php artisan serve
	y estará disponible a traves de la url http://127.0.0.1:8000
	
7.- Para actualizar el clima de la tabla ciudades debe ejecutar Comando de consola  

		php artisan citiesWeather:get


		
	
	

