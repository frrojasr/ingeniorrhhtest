<?php

namespace Modules\WeatherMap\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\WeatherMap\Entities\City;
use Modules\WeatherMap\Entities\CityWeather;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class GetWeatherCities extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'citiesWeather:get';
    protected $apikey = 'b559b7c5d4dbbcc555b55b7104ab2908';
    protected $endpoint = 'api.openweathermap.org/data/2.5/weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtiene los valores del clima desde la API de openweathermap.org y la guarda en la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //obtengo la lista de ciudades
        $cities = City::All();

        $horaActual = Carbon::now();

        foreach ($cities as $city) {

            $lastRecord = $city->myWeatherRecords()->orderBy('date_fetch', 'desc')->first(); //en cada ciudad obtengo su ultimo resgistro del tiempo
            $minutos = 60;

            if ($lastRecord) { //si tengo un ultimo de registro del tiempo, veo cuanto minutos han trascurrido desde entonces.
                $ultima = Carbon::parse($lastRecord->date_fetch);
                $minutos = $horaActual->diffInMinutes($ultima);
            }

            if ($minutos > 59) { //si el ultimo estado de tiempo fue tomado hace mas de una hora
                //lo vuelvo a tomar 
                $client = new Client();
//                $params = [
//                    'id' => $city->id,
//                    'appid' => $this->apikey
//                ];


//                $headers = [
//                    'api-key' => $this->apikey
//                ];

                $response = $client->request('GET', $this->endpoint.'?id='.$city->id.'&appid='.$this->apikey, [
                    //'json' => $params,
//                    'headers' => $headers,
                    'verify' => false,
                ]);

                $responseBody = json_decode($response->getBody());


//                dd($responseBody->main->temp);


                $newRecord = new CityWeather;
                $newRecord->id_city = $city->id;
                $newRecord->date_fetch = $newRecord->created_at = $newRecord->updated_at = Carbon::now();
                $newRecord->temp = $responseBody->main->temp;
                $newRecord->temp_min = $responseBody->main->temp_min;
                $newRecord->temp_max =$responseBody->main->temp_max;
                $newRecord->pressure = $responseBody->main->pressure;
                $newRecord->humidity = $responseBody->main->humidity;
                $newRecord->save(); // grabar y los muestro
                $lastRecord = $newRecord;
            }
            $this->info('Ciudad: ' . $city->name . ' Temperatura:' . $lastRecord->temp . ' Uitima lectura ' . $lastRecord->date_fetch);



            //
        }
//        $this->info(Module::get('WeatherMap'));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
//            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return [
//            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
