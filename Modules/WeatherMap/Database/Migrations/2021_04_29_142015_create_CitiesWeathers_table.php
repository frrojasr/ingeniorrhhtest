<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesWeathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CitiesWeathers', function (Blueprint $table) {
            $table->bigInteger('id_city');
            $table->datetime('date_fetch');
            $table->double('temp');
            $table->double('temp_max');
            $table->double('temp_min');
            $table->double('pressure');
            $table->double('humidity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CitiesWeathers');
    }
}
