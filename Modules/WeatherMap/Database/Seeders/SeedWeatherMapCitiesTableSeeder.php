<?php

namespace Modules\WeatherMap\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeedWeatherMapCitiesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('cities')->insert([
                ['id' => 3688689, 'name' => 'Bogotá', 'country' => 'CO', 'lon' => 4.6097, 'lat' => -74.0817, 'created_at' => now(), 'updated_at' => now()], 
                ['id' => 3674962, 'name' => 'Medellín', 'country' => 'CO', 'lon' => 6.2518, 'lat' => -75.5636, 'created_at' => now(), 'updated_at' => now()], 
                ['id' => 3911925, 'name' => 'La Paz', 'country' => 'BO', 'lon' => -16.5, 'lat' => -68.15, 'created_at' => now(), 'updated_at' => now()], 
                ['id' => 3652462, 'name' => 'Quito', 'country' => 'EC', 'lon' => -0.2299, 'lat' => -78.5249, 'created_at' => now(), 'updated_at' => now()],
                ['id' => 3117735, 'name' => 'Madrid', 'country' => 'ES', 'lon' => 40.4165, 'lat' => -3.7026, 'created_at' => now(), 'updated_at' => now()], 
                ['id' => 2988507, 'name' => 'Paris', 'country' => 'ES', 'lon' => 48.8534, 'lat' => -2.3488, 'created_at' => now(), 'updated_at' => now()],
                ['id' => 1850144, 'name' => 'Tokio', 'country' => 'JP', 'lon' => 35.6895, 'lat' => -139.6917, 'created_at' => now(), 'updated_at' => now()]]
        );
    }

}
