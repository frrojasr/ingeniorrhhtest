<?php

namespace Modules\WeatherMap\Entities;

use Illuminate\Database\Eloquent\Model;

class CityWeather extends Model
{
    
    protected $table = 'citiesweathers';
    protected $fillable = ['id_city', 'date_fetch', 'temp', 'temp_max', 'temp_min', 'pressure', 'humidity'];
}
