<?php

namespace Modules\WeatherMap\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\WeatherMap\Entities\CityWeather;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = [];
    
    
    public function myWeatherRecords() {
        return $this->hasMany(CityWeather::class, 'id_city', 'id');
    }
}
